FROM docker.io/library/nginx:1.19.6

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/nginx.key -out /etc/ssl/nginx.crt -subj '/CN=www.mydom.com/O=My Company Name LTD./C=TN'

RUN openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
RUN echo '127.0.0.1 local-server-01' >> /etc/hosts
RUN echo '127.0.0.1 local-server-02' >> /etc/hosts

ARG CONF_DIR=/opt/app/src
ENV CONF_DIR=$CONF_DIR

COPY ./conf/self-signed.conf $CONF_DIR/snippets/self-signed.conf
COPY ./conf/ssl-params.conf $CONF_DIR/snippets/ssl-params.conf
COPY ./conf/default.conf $CONF_DIR/conf.d/default.conf
COPY ./conf/nginx.conf $CONF_DIR/nginx.conf
RUN chown -R nginx:nginx $CONF_DIR

CMD ["nginx", "-g", "daemon off;", "-c", "/opt/app/src/nginx.conf"]
